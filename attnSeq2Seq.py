
import torch
import torch.nn as nn
from torch.autograd import Variable
import torch.nn.functional as F
from torch.nn.utils.rnn import pack_padded_sequence, pad_packed_sequence


class Encoder(nn.Module):
    def __init__(self, vocab_size, embed_size, hidden_size, padding_idx,
                 dropout_ratio, bidirectional=False, num_layers=1):
        super(Encoder, self).__init__()
        self.embedding = nn.Embedding(vocab_size, embed_size, padding_idx)
        self.drop = nn.Dropout(p=dropout_ratio)

        self.num_directions = bidirectional + 1
        self.lstm = nn.LSTM(embed_size, hidden_size, num_layers,
                            batch_first=True, dropout=dropout_ratio, bidirectional=bidirectional)

        self.tanh = nn.Tanh()
        self.linear = nn.Linear(hidden_size * self.num_directions,
                                hidden_size * self.num_directions
                                )

    def forward(self, inputs, lengths):
        embeds = self.embedding(inputs)
        embeds = self.drop(embeds)

        packed_embeds = pack_padded_sequence(embeds, lengths, batch_first=True)
        enc_h, (encoder_ht, encoder_ct) = self.lstm(packed_embeds)

        if self.num_directions == 2:
            h_t = torch.cat((encoder_ht[-1], encoder_ht[-2]), 1)
            c_t = torch.cat((encoder_ct[-1], encoder_ct[-2]), 1)
        else:
            h_t = encoder_ht[-1]
            c_t = encoder_ct[-1]

        h_t = self.tanh(self.linear(h_t))

        ctx, _ = pad_packed_sequence(enc_h, batch_first=True)
        ctx = self.drop(ctx)
        return ctx, h_t, c_t

class SoftDotAttention(nn.Module):
    '''Soft Dot Attention.

    Ref: http://www.aclweb.org/anthology/D15-1166
    Adapted from PyTorch OPEN NMT.
    '''

    def __init__(self, dim):
        '''Initialize layer.'''
        super(SoftDotAttention, self).__init__()
        self.linear_in = nn.Linear(dim, dim, bias=False)
        self.sm = nn.Softmax(dim=1)
        self.linear_out = nn.Linear(dim * 2, dim, bias=False)
        self.tanh = nn.Tanh()

    def forward(self, h, context, mask=None):
        '''Propagate h through the network.

        h: batch x dim
        context: batch x seq_len x dim
        mask: batch x seq_len indices to be masked
        '''
        target = self.linear_in(h).unsqueeze(2)  # batch x dim x 1

        # Get attention
        attn = torch.bmm(context, target).squeeze(2)  # batch x seq_len
        if mask is not None:
            # -Inf masking prior to the softmax
            attn.data.masked_fill_(mask, -float('inf'))
        attn = self.sm(attn)
        attn3 = attn.view(attn.size(0), 1, attn.size(1))  # batch x 1 x seq_len

        weighted_context = torch.bmm(attn3, context).squeeze(1)  # batch x dim
        h_tilde = torch.cat((weighted_context, h), 1)

        h_tilde = self.tanh(self.linear_out(h_tilde))
        return h_tilde, attn


class Decoder(nn.Module):
    def __init__(self, input_action_size, output_action_size, embed_size, hidden_size,
                 dropout_ratio, feature_size=2048):
        super(Decoder, self).__init__()
        self.embedding = nn.Embedding(input_action_size, embed_size)
        self.lstm = nn.LSTMCell(embed_size + feature_size, hidden_size)
        self.attn_layer = SoftDotAttention(hidden_size)
        self.drop = nn.Dropout(p=dropout_ratio)
        self.linear = nn.Linear(hidden_size, output_action_size)

    def forward(self, action, img_feature, h_0, c_0, ctx=None, ctx_mask=None):
        action_embeds = self.embedding(action)
        action_embeds = action_embeds.squeeze()
        inputs = torch.cat((action_embeds, img_feature), 1)
        inputs = self.drop(inputs)
        h_1, c_1 = self.lstm(inputs, (h_0, c_0))
        h1_dp = self.drop(inputs)
        h_tilde, alpha = self.attention_layer(h1_dp, ctx, ctx_mask)
        logit = self.linear(h_tilde)
        return h_1, c_1, alpha, logit

